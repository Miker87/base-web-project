var gulp = require('gulp');
var babel = require('gulp-babel');
var concat = require('gulp-concat');
var connect = require('gulp-connect');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');


gulp.task('connect', function() {
    connect.server({
        root: './src',
        livereload: true
    });
});



gulp.task('compileSASS', function () {
    return gulp.src('./src/assets/stylesheets/styles.scss')
        .pipe(sass()
            .on('error', sass.logError))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./src/assets/stylesheets'))
        .pipe(connect.reload());
});

gulp.task('compileJS', function() {
    gulp.src(['src/**/*.js', '!src/**/all.js'])
        .pipe(sourcemaps.init())
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(concat('all.js'))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./src/js'))
        .pipe(connect.reload());
});

gulp.task('watchJS', function() {
    gulp.watch('src/**/*.js', ['compileJS']);
});

gulp.task('watchSASS', function() {
    gulp.watch('src/**/*.scss', ['compileSASS']);
});


gulp.task('default', ['connect', 'compileJS', 'compileSASS', 'watchJS', 'watchSASS']);