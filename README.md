## Synopsis
Basic Web project to get Web developers started with ES6 and SASS.

## Motivation
Not having to do to this all over again to test something, like a framework.

## Requirements
- Npm

## Installation
- Run `npm install`
- Run `gulp` to run the project on localhost:8080

## License
MIT License